json.array!(@items) do |item|
  json.extract! item, :id, :barkod, :naslov, :ime_autora, :prezime_autora, :kategorija, :status, :mjesto, :godina, :dobavljac, :vrijednost, :signatura, :napomena, :izdavac
  json.url item_url(item, format: :json)
end
