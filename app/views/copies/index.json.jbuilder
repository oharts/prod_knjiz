json.array!(@copies) do |copy|
  json.extract! copy, :id, :barkod_knjige, :item_id
  json.url copy_url(copy, format: :json)
end
