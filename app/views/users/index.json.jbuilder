json.array!(@users) do |user|
  json.extract! user, :id, :ime, :prezime, :ulica, :grad, :PB, :email, :telefon1, :telefon2, :organizacija, :napomena
  json.url user_url(user, format: :json)
end
