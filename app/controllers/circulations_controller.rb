class CirculationsController < ApplicationController
    
    before_action :set_circulation, only: [:show, :edit,  :destroy]
before_action :authenticate_admin!   
    def new
     @circ=Circulation.new
    
     
    end
    
    def index
        @circs = Circulation.all
    end
     
      def edit
      end
      
      
      
      def show
      end
    
    
    def create
        
        change_params
        @i=params[:circulation][:copy_id]
        if (!@i.blank? && !Copy.where("barkod_knjige='#{@i}'").first.blank? && !User.where("id=#{params[:circulation][:user_id]}").first.blank?)
            
        @t= Copy.where("barkod_knjige=#{@i}").first.id
     
    
    
    
     if (Circulation.posudjeno.where("copy_id=#{@t}").empty?)
         @circ=Circulation.new(circulation_params)
         flash[:success] = "Knjiga je posuđena!"
        if(!@circ.save)
            flash[:danger] = "ohoho"
        end
     else
        flash[:danger] = "Knjiga je već posuđena"
     end
 else
     flash[:danger] = "Krivo unešen barkod knjige ili korisnika!!!"
     
 end
 
        redirect_to :action => :new 
    
    end
    
    
    
    
    
def update
       @i=params[:q] 
       if (!Copy.where("barkod_knjige='#{@i}'").first.blank?) 
      @t= Copy.where("barkod_knjige='#{@i}'").first.id
      if (!Circulation.posudjeno.where("copy_id=#{@t}").first.blank?)
    Circulation.posudjeno.where("copy_id='#{@t}'").first.update(:indikacija => 2)
    flash[:success] = "Uspješno vraćena knjiga"
    else
    flash[:danger] = "Nije posuđena!!!"
    end
else
    flash[:danger] = "Krivi barkod!!!"
    end
    
    redirect_to vracanje_path 
end

    
    def posudjene
    end
    
    def vracanje
    end
    
    
    def destroy
    @circ.destroy
    respond_to do |format|
      format.html { redirect_to circulations_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
    
    private
    # Use callbacks to share common setup or constraints between actions.
   def change_params
       @c=params[:circulation][:copy_id]
     if (!@c.blank? && !Copy.where("barkod_knjige='#{@c}'").first.blank?)
        cp = circulation_params
        cp[:copy_id] = Copy.where("barkod_knjige=#{@c}").first.id
    
  end
   end

    # Never trust parameters from the scary internet, only allow the white list             through.
    def circulation_params
      @circulation_params ||=params.require(:circulation).permit(:copy_id, :user_id, :indikacija)
    end
    
    def set_circulation
      @circ = Circulation.find(params[:id])
    end
    
    
    
   
    
    
end
