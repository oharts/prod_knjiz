class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
before_action :authenticate_admin!
  # GET /items
  # GET /items.json
  def index
    if params[:q].blank?
    @q = Item.none.search
    else
   @q = Item.search(params[:q])
    end
     @items = @q.result.paginate(page: params[:page], :per_page => 10)
     
    respond_to do |format|
      format.html
      format.pdf do 
        
        pdf = KnjigePdf.new(@items, view_context)
        
       
send_data pdf.render,filename: "mate.pdf",
                         type: "application/pdf",
                         disposition: "inline"
                        
                         

      end
    end
    
  end
  
  def inv_knjiga
  @items=Item.all
  respond_to do |format|
     
      format.pdf do 
        
        pdf = KnjigePdf.new(@items, view_context)
        
       
send_data pdf.render,filename: "Inventarna knjiga.pdf",
                         type: "application/pdf",
                         disposition: "inline"
                        
                         

      end
    end
  
  end

  # GET /items/1
  # GET /items/1.json
  def show
  end
  
  def search
    index
    render :search
  end
  
  def povijest
     @item = Item.find(params[:id])
  end
  

  # GET /items/new
  def new
    @item = Item.new
    @item.copies.build
  end
  
  def svi
    @items = Item.paginate(page: params[:page], :per_page => 10)
    @max=@items.total_entries
    
    @items_svi=Item.paginate(page:1, :per_page => @max)
    
    respond_to do |format|
      format.html
      format.pdf do 
        
        pdf = KnjigePdf.new(@items_svi, view_context)
        
          
send_data pdf.render,filename: "mate.pdf",
                         type: "application/pdf",
                         disposition: "inline"
                         

      end
    end
  end

  # GET /items/1/edit
  def edit
    @item.copies.build
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Uspješno stvorena nova stavka.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Stavka je uspješno uređena.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def posudjene
    @item = Circulation.posudjeno.paginate(page: params[:page], :per_page => 10)
    @itemposu=Circulation.posudjeno.size
  end
  
  def knjiznifond
  end
  
  def statistikapogod
    @options = ["2017", "2016", "2015", "2014"]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:barkod, :naslov, :ime_autora, :image, :prezime_autora, :kategorija, :status, :mjesto, :godina, :dobavljac, :vrijednost, :signatura, :napomena, :izdavac, copies_attributes: [:id, :barkod_knjige, :_destroy])
    end
end
