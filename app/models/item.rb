class Item < ActiveRecord::Base
    has_many :copies, dependent: :destroy
    accepts_nested_attributes_for :copies, reject_if: proc { |attributes| attributes['barkod_knjige'].blank? }, allow_destroy: true
     has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing-book.png"
   validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end 
