class User < ActiveRecord::Base
    has_many :circulations
    has_many :copies, :through => :circulations
end
