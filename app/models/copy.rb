class Copy < ActiveRecord::Base
 
 
 
  belongs_to :item
  has_many :circulations
  has_many :users, :through => :circulations
end
