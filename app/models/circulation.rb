class Circulation < ActiveRecord::Base
  
    belongs_to :user
    belongs_to :copy
    scope :posudjeno, -> { where("created_at = updated_at")}
    scope :vraceno, -> { where("created_at < updated_at")}
end
