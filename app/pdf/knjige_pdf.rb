class KnjigePdf < Prawn::Document
    def initialize(item, view)
        super(:top_margin =>20,:page_layout => :landscape)
        @mate=item
        @view=view
        font("#{Rails.root}/app/assets/fonts/VERDANA.TTF")
       text "INVENTARNA KNJIGA" , :size => 20

        line_items
   
    end
    
    def line_items
        @x=[[""]]
         @x.delete_at(0)
    move_down 20
@mate.map{ |f| line_item_rows(f)}
table table_header+@x do
     self.cell_style = { size: 7 }
     self.width = 730
     self.column_widths = {0 => 40}
    self.header = true
    self.row_colors=["CCFF99", "FFFFFF"]
    row(0).font= "#{Rails.root}/app/assets/fonts/VERDANAB.TTF"
  row(0).size = 7
  

end
    end
    
    
    def line_item_rows(f)
  
   
         f.copies.map do |g|
    @x= [[g.barkod_knjige.to_i, g.item.created_at.strftime("%d %m %Y"), "#{g.item.prezime_autora}, #{g.item.ime_autora}",
    g.item.naslov, g.item.mjesto, g.item.izdavac, g.item.godina, g.item.dobavljac, g.item.vrijednost,
    g.item.signatura, g.item.napomena]]+@x
 @x.sort!

 
   end
    end
    
    def table_header
        [["Inv. broj","Datum", "Prezime i ime autora", "Naslov", "Mjesto izdanja", "Nakladnik", "Godina", "Dobavljač", "Cijena", "Signatura", "Napomena"]]
     
    end
    
    
end
