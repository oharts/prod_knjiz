class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :ime
      t.string :prezime
      t.string :ulica
      t.string :grad
      t.string :PB
      t.string :email
      t.string :telefon1
      t.string :telefon2
      t.string :organizacija
      t.text :napomena

      t.timestamps null: false
    end
  end
end
