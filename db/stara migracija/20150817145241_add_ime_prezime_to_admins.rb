class AddImePrezimeToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :ime, :string
    add_column :admins, :prezime, :string
  end
end
