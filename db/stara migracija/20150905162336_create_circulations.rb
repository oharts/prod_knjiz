class CreateCirculations < ActiveRecord::Migration
  def change
    create_table :circulations do |t|
      t.integer :copy_id
      t.integer :user_id
      t.datetime :created_at

      t.timestamps null: false
    end
  end
end
