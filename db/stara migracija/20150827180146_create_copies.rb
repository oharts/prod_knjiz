class CreateCopies < ActiveRecord::Migration
  def change
    create_table :copies do |t|
      t.string :barkod_knjige
      t.references :item, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
