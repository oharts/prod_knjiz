class RemoveIdKnjigeFromCopy < ActiveRecord::Migration
  def change
    remove_column :copies, :ID_knjige, :string
  end
end
