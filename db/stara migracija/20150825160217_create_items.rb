class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :barkod
      t.string :naslov
      t.string :ime_autora
      t.string :prezime_autora
      t.string :kategorija
      t.string :status
      t.string :mjesto
      t.string :godina
      t.string :dobavljac
      t.string :vrijednost
      t.string :signatura
      t.text :napomena
      t.string :izdavac

      t.timestamps null: false
    end
  end
end
