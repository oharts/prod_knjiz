class RenameItemIdInCopy < ActiveRecord::Migration
  def change
    rename_column :copies, :item_ID, :item_id
  end
end
