class CreateKategorijas < ActiveRecord::Migration
  def change
    create_table :kategorijas do |t|
      t.string :kategorija

      t.timestamps null: false
    end
  end
end
