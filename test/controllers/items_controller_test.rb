require 'test_helper'

class ItemsControllerTest < ActionController::TestCase
  setup do
    @item = items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create item" do
    assert_difference('Item.count') do
      post :create, item: { barkod: @item.barkod, dobavljac: @item.dobavljac, godina: @item.godina, ime_autora: @item.ime_autora, izdavac: @item.izdavac, kategorija: @item.kategorija, mjesto: @item.mjesto, napomena: @item.napomena, naslov: @item.naslov, prezime_autora: @item.prezime_autora, signatura: @item.signatura, status: @item.status, vrijednost: @item.vrijednost }
    end

    assert_redirected_to item_path(assigns(:item))
  end

  test "should show item" do
    get :show, id: @item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @item
    assert_response :success
  end

  test "should update item" do
    patch :update, id: @item, item: { barkod: @item.barkod, dobavljac: @item.dobavljac, godina: @item.godina, ime_autora: @item.ime_autora, izdavac: @item.izdavac, kategorija: @item.kategorija, mjesto: @item.mjesto, napomena: @item.napomena, naslov: @item.naslov, prezime_autora: @item.prezime_autora, signatura: @item.signatura, status: @item.status, vrijednost: @item.vrijednost }
    assert_redirected_to item_path(assigns(:item))
  end

  test "should destroy item" do
    assert_difference('Item.count', -1) do
      delete :destroy, id: @item
    end

    assert_redirected_to items_path
  end
end
