Rails.application.routes.draw do


 
 resources :circulations
 match 'vracanje' => 'circulations#vracanje', via: [:get, :post]
 
 resources :copies

 resources :items do
   collection do
      match 'search' => 'items#search', via: [:get, :post], as: :search
      get 'svi'
      get '/:id/povijest' => 'items#povijest', as: :povijest
      get 'inv_knjiga' => 'items#inv_knjiga', as: :inv_knjiga
      get 'posudjene'
      match 'fond' => 'items#knjiznifond', via: [:get, :post], as: :fond 
      match 'statistikapogod' => 'items#statistikapogod', via: [:get, :post], as: :statistikapogod 
      end
  end
 
 resources :users do
      collection do
        match 'search' => 'users#search', via: [:get, :post], as: :search
        get 'svi'
        get '/:id/povijest' => 'users#povijest', as: :povijest
  end


end



  devise_for :admins
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  devise_scope :admin do
  #root 'devise/sessions#create'
    get "/login" => "devise/sessions#new"
    get "/logout" => "devise/sessions#destroy"
   end
  get "/static/st"
  get "/static/sa"
  get "/test" => "users#prazno"

 
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
